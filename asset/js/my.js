$(document).ready(function(){
	initWiki();
    setTimeout(scrollTo, 100, 0, 1);
});

function showRandomWiki(data)
{
    if(data){
		var title = data.query.random[0].title;
		var atag = document.getElementById("wikilink");		
		var dom = document.getElementById("wiki");
		if(!atag){ 
			atag = document.createElement("a");		
		}
		atag.href = "http://ja.wikipedia.org/wiki/" + title;
		atag.id = "wikilink"
		atag.innerHTML = title;
		dom.appendChild(atag);
    }
    else{
    	alert("Error!");
    }
}

function initWiki(){
	var delscr = document.body.getElementsByTagName("script");
	for(var i =0; i < delscr.length; i++){
		document.body.removeChild(delscr[i]);
	}
	var atag = document.getElementById("wikilink");		
	if(atag){
		document.getElementById("wiki").removeChild(atag);	
	}
}

function clearWiki(){
	initWiki();
}

function getRandomWiki() {
	initWiki();
	var script = document.createElement("script");
	var url ="http://ja.wikipedia.org/w/api.php?action=query&list=random&rnlimit=1&rnnamespace=0&callback=showRandomWiki&format=json";
    var date = new Date();
    script.src= url + "&yoski=" + date.getTime();
    document.body.appendChild(script);
}

