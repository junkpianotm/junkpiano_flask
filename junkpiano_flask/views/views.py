from flask import render_template
from junkpiano_flask import app

@app.route('/')
def hello_world():
	return render_template('top.html')

@app.route('/deathmail_cd')
def d_cd():
	return render_template('deathmail_cd.html') 

@app.route('/add')
def add():
	return 'add test'

@app.route('/diary')
def diary():
	return render_template('diary/top.html')
